import bs4
import requests

url = "http://www.bloomberg.com/europe"

urls = [url] # stack of urls for crawling
visited = {url} # historic record of urls
articles = set()

f = open("whole_bloomberg.txt", 'w')
f_a = open ("just_a.txt", "w")

while len(urls) > 0:
    try:
        htmltext = requests.get(urls[0]).text
    except:
        print(urls[0])
    soup = bs4.BeautifulSoup(htmltext, 'html.parser')
    print("_____")
    print(len(urls))
    print(len(articles))
    tmp = urls.pop(0)

    for link in soup.find_all('a'):
        got = str(link.get('href'))
        if link in soup.find_all('a'):
            got = str(link.get('href'))
            if "http" not in got:
                got = tmp + got
            if got not in visited and "www.bloomberg.com" in got :
                urls.append(got)
                visited.add(got)
                print(got, file = f)
                if "/articles/" in got and got not in articles:
                    print(got, file = f_a) 
                    articles.add(got)
f.close()
f_a.close()

